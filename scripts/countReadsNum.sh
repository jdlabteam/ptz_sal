#!/bin/bash

ID=$( sed -n ${SLURM_ARRAY_TASK_ID}p /scratch/jdlab/yating/PTZ_Sal/code/jobs/lookup.txt )

projDir=/scratch/jdlab/yating/PTZ_Sal

printf "Barcode,num_reads_R1,num_reads_R2,num_reads_after_trim_R1,num_reads_after_trim_R2,num_reads_after_filter_rrna_R1,num_reads_after_filter_rrna_R2,num_reads_unique_mapped\n"

for i in $ID; 
	do printf '%s,' $i; 
	printf '%s,' $(zcat "${projDir}/activity_dep_astro_transl_trap_rnaseq/${i}_R1_001.fastq.gz" | echo $((`wc -l`/4)));
	printf '%s,' $(zcat "${projDir}/activity_dep_astro_transl_trap_rnaseq/${i}_R2_001.fastq.gz" | echo $((`wc -l`/4)));
	printf '%s,' $(zcat "${projDir}/results/trimmed/${i}_trimmed_1P.fastq.gz" | echo $((`wc -l`/4)));
	printf '%s,' $(zcat "${projDir}/results/trimmed/${i}_trimmed_2P.fastq.gz" | echo $((`wc -l`/4)));
	printf '%s,' $(zcat "${projDir}/results/post_rrna_filter/${i}_no_rRNA.fastq.1.gz" | echo $((`wc -l`/4)));
	printf '%s,' $(zcat "${projDir}/results/post_rrna_filter/${i}_no_rRNA.fastq.2.gz" | echo $((`wc -l`/4)));
	printf '%s\n' $(grep "Uniquely mapped reads number" "${projDir}/results/mapped/${i}_Log.final.out" | cut -f2 ); 
done