# script to extract rRNAs from ncRNAs and output a new Fasta with only rRNAs
## python extract_rrna.py ncRNA.fasta rRNA.fasta

import sys

if len(sys.argv) < 3:
	raise("Pass input Fasta and output Fasta")
input = sys.argv[1]
output = sys.argv[2]
out = open(output, "w")
f = open(input, "r")

w = False
lines = f.readlines()

for l in lines:
    if ">" in l:
        if "gene_biotype:rRNA transcript_biotype:rRNA" in l:
            w = True
        else:
            w = False
    if w:
        out.write(l)

out.close()
f.close()