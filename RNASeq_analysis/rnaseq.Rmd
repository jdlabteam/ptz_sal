---
title: "Activity dependent translation in astrocytes"
author: "Yating Liu"
date: "7/23/2019"
output: html_document
---

```{r setup, include=FALSE, message=FALSE}
knitr::opts_knit$set(root.dir = "~/Documents/Genetics/Paper_related_analysis/PTZ_Sal")
knitr::opts_chunk$set(echo = TRUE)

library("edgeR")
library("biomaRt")
library("ggplot2")
library("data.table")
library("combinat")
library("gridExtra")
library("gplots")
library("plyr")
library("Rtsne")
library("stringr")
library("ggforce")
library("readr")
library("dplyr")
library("ggpubr")
library("tidyverse")
library("VennDiagram")

options(stringsAsFactors = F)
save_figs <- F
```

# Read HTSeq counts and generate dge object 
```{r}
files <- dir(path = "results/htseq_count/", pattern = "*htseq.txt$")
counts <- readDGE(files, path = "results/htseq_count")$counts
# filter out metadata fields in counts 
noint <- rownames(counts) %in% c("__no_feature","__ambiguous","__too_low_aQual",
                                "__not_aligned","__alignment_not_unique")
counts <- counts[!noint,]
if (!file.exists("RNASeq_analysis/objs/genes_97.rds")){
# get gene information from ensembl
ensembl <- useEnsembl(biomart = "ENSEMBL_MART_ENSEMBL", version = 97, dataset = "mmusculus_gene_ensembl")
stopifnot(length(unique(rownames(counts))) == nrow(counts))
gtf.ens <- getBM(attributes=c('ensembl_gene_id','external_gene_name',"gene_biotype"),
                 filters = 'ensembl_gene_id', values = rownames(counts), mart = ensembl)
gtf.ens <- as.data.table(gtf.ens)
saveRDS(gtf.ens, "RNASeq_analysis/objs/genes_97.rds")
}
genes <- readRDS("RNASeq_analysis/objs/genes_97.rds")
genes <- genes[match(rownames(counts), genes$ensembl_gene_id),]
stopifnot(genes$ensembl_gene_id == rownames(counts))
## reorder columns based on samples 
colnames(counts) <- str_remove(colnames(counts), "_htseq")
group <- sapply(str_split(colnames(counts), "-"), `[[`, 1)
replicate <- sapply(str_split(colnames(counts), "-"), `[[`, 2)
samples <- data.frame(sample = colnames(counts), group = group, replicate = paste("rep", replicate, sep = "_"), 
                      treatment = c(rep("Ptz", 6), rep("Sal", 6)), 
                      celltype = c(rep("IP",3), rep("Pre", 3), rep("IP",3), rep("Pre", 3)))

dge <- DGEList(counts = counts, 
               group = samples$group, 
               genes = genes
)
dge$samples$group <- samples$group
dge$samples$replicate <- samples$replicate
dge$samples$celltype <- samples$celltype
dge$samples$treatment <- samples$treatment
# store only protein coding genes for later analysis
dge <- dge[dge$genes$gene_biotype=="protein_coding",]
saveRDS(dge, "RNASeq_analysis/objs/dge_protein_coding.rds")

# filter out lowly expressed genes
dge <- readRDS("RNASeq_analysis/objs/dge_protein_coding.rds")
## library size ~ 5*10e6, so cpm(dge)>5 filter genes with count > ~25
keep <- rowSums(cpm(dge)>5) >= 3
table(keep)
dge <- dge[keep, , keep.lib.sizes=FALSE]
# normalizes for RNA composition
dge <- calcNormFactors(dge)
saveRDS(dge, "RNASeq_analysis/objs/dge_filt_norm.rds")
```

# Read dge object and generate log2(cpm) table
```{r}
dge <- readRDS("RNASeq_analysis/objs/dge_filt_norm.rds")
# get the log2(cpm) 
cpm <- data.table(dge$genes, cpm(dge, log = T))
cpm <- cpm[,-3,with=FALSE]
write.csv(cpm, "RNASeq_analysis/reports/cpm.csv", row.names = F)
```

# QC plots

## Reproducibility
### MDS plots
```{r mds}

dge$samples$group <- factor(dge$samples$group, levels = c("PtzIP", "PtzPreIP", "SalIP", "SalPreIP"))
dge$samples$treatment <- factor(dge$samples$treatment, levels = c("Ptz", "Sal"))
dge$samples$celltype <- factor(dge$samples$celltype, levels = c("IP", "Pre"))

colors<-c('deepskyblue', 'brown4', 'chocolate2', 'darkorchid2')
if (save_figs) pdf("RNASeq_analysis/figs/MDS_dim_1_2.pdf")
plotMDS(dge,col=colors[factor(dge$samples$group)], dim.plot = c(1,2), xlab="logFC dim1", ylab="logFC dim2")
legend(0,4, legend=levels(dge$samples$group), col=colors, cex=0.6, pch=8)
if (save_figs) dev.off()

if (save_figs) pdf("RNASeq_analysis/figs/MDS_dim_1_3.pdf")
plotMDS(dge,col=colors[factor(dge$samples$group)], dim.plot = c(1,3), xlab="logFC dim1", ylab="logFC dim3")
legend(0,-1.2, legend=levels(dge$samples$group), col=colors, cex=0.6, pch=8)
if (save_figs) dev.off()

if (save_figs) pdf("RNASeq_analysis/figs/MDS_dim_2_3.pdf")
plotMDS(dge,col=colors[factor(dge$samples$group)], dim.plot = c(2,3), xlab="logFC dim2", ylab="logFC dim3")
legend(4,-2, legend=levels(dge$samples$group), col=colors, cex=0.6, pch=8)
if (save_figs) dev.off()
```

### Hierarchical Clustering

```{r}
require(gplots)

mat <- as.matrix(cpm[, 3:14, with=FALSE])
mat <- mat[rowSums(!is.finite(mat))==0,]

if (save_figs) pdf("RNASeq_analysis/figs/heatmap_log2.pdf")
heatmap.2(mat, 
          distfun = function(x) as.dist((1-cor(t(x)))/2),
          dendrogram = "column",
          labRow = "",
          #col = colorRampPalette(c("white","grey","black"))(n=100),
          colCol = colors[factor(dge$samples$group)],
          trace = "none",
          density.info = "none",
          margins = c(15,2))
if (save_figs) dev.off()
```

## add markers to cpm table
```{r}

astro_spec <- readRDS("RNASeq_analysis/objs/astro_spec_microarray.rds")
neuron_spec <- readRDS("RNASeq_analysis/objs/neuron_spec_microarray.rds")

cpm <- cpm[ensembl_gene_id %in% astro_spec, marker := "astro"]
cpm <- cpm[ensembl_gene_id %in% neuron_spec, marker := "neuron"]
cpm$marker <- factor(cpm$marker, levels = c("astro", "neuron"))
```

### XY scatters

#### reshape data
```{r}
cpm.long <- melt(cpm, id.vars=c(1:2,15), measure.vars=3:14, variable.name="sample", value.name="CPM")
cpm.long <- cpm.long[grep("PreIP", sample), group.cell_type := "PreIP"] 
cpm.long <- cpm.long[!grep("PreIP", sample), group.cell_type := "IP"] 
cpm.long <- cpm.long[grep("Ptz", sample), group.treatment := "Ptz"]
cpm.long <- cpm.long[grep("Sal", sample), group.treatment := "Sal"]
cpm.long <- cpm.long[, group.replicate := paste0("rep_", sapply(strsplit(as.vector(cpm.long$sample), "-"), tail, 1))]

cpm.long$group.cell_type <- factor(cpm.long$group.cell_type, levels = c("PreIP", "IP"))
cpm.long$group.treatment <- factor(cpm.long$group.treatment, levels = c("Ptz", "Sal"))
cpm.long$group.replicate <- factor(cpm.long$group.replicate, levels = c("rep_1", "rep_2", "rep_3"))
```

### XY scatters: reproducibility

```{r}
cpm.by_replicate <- dcast(cpm.long, ensembl_gene_id + group.cell_type + group.treatment ~ group.replicate, value.var = "CPM")
cpm.by_replicate$group.cell_type <- factor(cpm.by_replicate$group.cell_type, labels = c("PreIP", "IP"))
cpm.by_replicate$group.treatment <- factor(cpm.by_replicate$group.treatment, labels = c("Ptz", "Sal"))

cor_ <- function(df, rep1, rep2) {
  stopifnot(c(rep1, rep2) %in% names(df))
  return(paste0("r = ", format(cor(df[, rep1], df[, rep2]), digits = 3)))
}
require("plyr")

# make two plots for treatment == "Ptz", and treatment == "Sal"
celltypes <- levels(cpm.by_replicate$group.cell_type)
stopifnot(celltypes == c("PreIP", "IP"))
treatments <- levels(cpm.by_replicate$group.treatment)
stopifnot(treatments == c("Ptz", "Sal"))


require("combinat")
comb.list <- list("Ptz" = as.matrix(combn(c("rep_1", "rep_2", "rep_3"), 2)), "Sal" = as.matrix(combn(c("rep_1", "rep_2", "rep_3"), 2)))

for(t in treatments){
  cpm.by_replicate.treatment <- cpm.by_replicate[cpm.by_replicate$group.treatment == t,]
  comb <- comb.list[[t]]
  plots <- list()
  init = 0
  colors<-list("PreIP" = "deepskyblue", "IP" = "darkorchid2")
  for(c in celltypes){
    cpm.table <- cpm.by_replicate.treatment[cpm.by_replicate.treatment$group.cell_type == c]
    for(i in 1:ncol(comb)){
      df.cor <- ddply(cpm.table, .(group.treatment, group.cell_type), cor_, comb[1, i], comb[2, i])
      #if (nrow(df.cor) == 0)
      #  next
      cpm.table.sub <- cpm.table[,c(1:3, grep(comb[1, i], colnames(cpm.table)), grep(comb[2, i], colnames(cpm.table))), with = FALSE]
      plots[[init + i]] <- ggplot(cpm.table.sub, aes_string(comb[1, i], comb[2, i])) +
        geom_point(alpha=.1, colour = colors[[c]]) +
        annotate("text", x=9, y = 0, label = df.cor[["V1"]], size = 3) +
        scale_color_manual(values=c("red","green3"), guide=FALSE) +
        theme_bw() +
        labs(x=paste(c, comb[1, i], t), y=paste(c, comb[2, i], t)) +
        theme(axis.text = element_text(size=6),
              axis.title = element_text(size=6),
              strip.text = element_text(size=6))
    }
    init = length(plots)
  }
  if (save_figs) pdf(paste0("RNASeq_analysis/figs/", t, "_scatterplot_reproducibility.pdf"))
  do.call(grid.arrange, c(plots, ncol=3))
  if (save_figs) dev.off()
}
```

## XY scatters: TRAP worked

### scatterplots of each TRAP vs PreIP

```{r trap_1}
cpm.by_tissue_type <- dcast(cpm.long, ensembl_gene_id + external_gene_name + group.replicate + group.treatment + marker ~ group.cell_type, value.var = "CPM")

#for (t in treatments) {
  #for (i in c(1:3)) {
    #dat <- cpm.by_tissue_type[group.replicate == paste0("rep_", i)]
dat <- cpm.by_tissue_type
  p <- ggplot(dat, aes(PreIP, IP)) +
  geom_point(col="grey55",alpha=.5) +
  geom_point(data=dat[marker %in% c("astro", "neuron")], size = 0.8, aes(col = factor(marker))) +
  scale_color_manual(values=c("red","green3"), guide=FALSE) +
  scale_shape_manual(values=c(1:20)) +
  geom_abline(intercept = 0, slope = 1) +
  #geom_abline(intercept = -1, slope = 1) +
  facet_grid(group.treatment~group.replicate) +
  theme_bw() +
  theme(axis.text = element_text(size=14),
        axis.title = element_text(size=14),
        strip.text = element_text(size = 14)) +
  labs(x="Log2(CPM) PreIP", y="Log2(CPM) IP")
if (save_figs) ggsave(paste0("RNASeq_analysis/figs/IP_vs_PRE_marker_genes_scatters.png"), plot = p)
  print(p)
 # }
#}
```

# Identify transcripts with at least 1.5X fold change in response to PTZ
## Selected all samples for differential analysis
```{r}
dge <- readRDS("RNASeq_analysis/objs/dge_filt_norm.rds")
```

## DE analsys: design matrix
```{r}
group <- factor(dge$samples$group)
design <- model.matrix(~0+group)
colnames(design) <- levels(group)
contrasts <- makeContrasts(
  PtzPreIP_vs_SalPreIP = PtzPreIP - SalPreIP,
  PtzIP_vs_PtzPreIP = PtzIP - PtzPreIP,
  SalIP_vs_SalPreIP = SalIP - SalPreIP,
  PtzIP_vs_SalIP = PtzIP - SalIP,
  IP_vs_Pre = (PtzIP + SalIP)/2 - (PtzPreIP + SalPreIP)/2,
  levels = design
)
```

## Explore data: BCV plot (Biological coefficient of variation)
```{r}
# estimate dispersions
dge <- estimateDisp(dge, design)
##  A common dispersion (i.e. red line on the BCV plot) between 0.2 and 0.4 is usually considered reasonable and hence could detect more DE genes.If the common dispersion is above the 0.4 threshold, this will influence the number of DE genes found in the study. 
message("common dispersion = ", dge$common.dispersion)
plotBCV(dge)
```

## Find differential expression
```{r}
fit <- glmFit(dge, design)

getDE <- function(contrast_name) {
  topTags(glmLRT(fit, contrast=contrasts[, contrast_name]), n=Inf, sort.by = "PValue")[[1]]
}
de <- lapply(attr(contrasts, 'dimnames')$Contrasts, getDE)
names(de) <- attr(contrasts, 'dimnames')$Contrasts

#lapply(de, function(xx) sum(xx$PValue<.05 & abs(xx$logFC) > log2(1.5) & xx$FDR < .1))
saveRDS(de, "RNASeq_analysis/objs/de.rds")

```
